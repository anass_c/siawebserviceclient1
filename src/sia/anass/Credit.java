package sia.anass;

public class Credit {

//	public String annuite(double capital, double taux, double duree) {
//		return (capital * taux / 100) / (1 - Math.pow(1 + taux / 100, -duree)) + "";
//	}

	public String annuite(double capital, double taux, double duree) {
//		double a = (Math.pow(1 + taux / 100, duree) * taux / 100 * capital) / (Math.pow(1 + taux / 100, duree) - 1);
		taux /= 100;
		double a;
		if (taux != 0)
			a = capital * taux / (1 - Math.pow((1 + taux), -duree));
		else
			a = capital / duree;
//		double tm = Math.pow(1 + taux/100, (double) 1 / 12) - 1;
//		double tm = taux/100;
//		double a = Math.pow(1 + tm, duree) * tm * capital / (Math.pow(1 + tm, duree) - 1);
		return (double) Math.round(a * 100) / 100 + "";

	}

	public String capital(double annuite, double taux, double duree) {
		taux /= 100;
		double c = annuite * (1 - Math.pow((1 + taux), -duree)) / taux;
		return (double) Math.round(c * 100) / 100 + "";
	}

	public String duree(double annuite, double taux, double capital) {
		double d = Math.log(-annuite / (taux / 100 * capital - annuite)) / Math.log(1 + taux / 100);
		return (int) Math.round(d * 100) / 100 + "";
	}

}
