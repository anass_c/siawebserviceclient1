package com.sia.anass.mvc.model;

import java.sql.Date;
import java.time.LocalDate;

public class CreditModel {

	private int idClient;

	private String capital, annuite, taux, duree;
	private Date date;

	public CreditModel() {
		date = Date.valueOf(LocalDate.now());
	}

	@Override
	public String toString() {
		return "CreditModel [idClient=" + idClient + ", capital=" + capital + ", annuite=" + annuite + ", taux=" + taux
				+ ", duree=" + duree + ", date=" + date + "]";
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getAnnuite() {
		return annuite;
	}

	public void setAnnuite(String annuite) {
		this.annuite = annuite;
	}

	public String getTaux() {
		return taux;
	}

	public void setTaux(String taux) {
		this.taux = taux;
	}

	public String getDuree() {
		return duree;
	}

	public void setDuree(String duree) {
		this.duree = duree;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
