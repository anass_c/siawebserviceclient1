package com.sia.anass.mvc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sia.anass.dbconfig.DBConnection;
import com.sia.anass.mvc.model.Client;
import com.sia.anass.mvc.model.CreditModel;

public class ClientDaoImp extends DBConnection implements ClientDao {

	@Override
	public Client login(Client user) {
		Client client = null;

		try {
			String query = "select * from TClient where NomCli=? and password=?";
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setString(1, user.getName());
			preparedStatement.setString(2, user.getPassword());
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				client = new Client();
				client.setId(resultSet.getInt("NumCli"));
				client.setName(resultSet.getString("NomCli"));
			}

			if (preparedStatement != null)
				preparedStatement.close();
			if (resultSet != null)
				resultSet.close();

		} catch (Exception e) {
			System.out.println("Connect to database: " + e);
		}

		disconnect();

		return client;
	}

	@Override
	public void insertCredit(CreditModel c) {
		String query = "INSERT INTO `TCredit` (`NumCre`, `DatCre`, `MonCre`, `DurCre`, `TauCre`, `AnnCre`, `DatPreVer`, `NumCli`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)";

		try {
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			preparedStatement.setDate(1, c.getDate());
			preparedStatement.setString(2, String.valueOf(c.getCapital()));
			preparedStatement.setString(3, String.valueOf(c.getCapital()));
			preparedStatement.setString(4, String.valueOf(c.getTaux()));
			preparedStatement.setString(5, String.valueOf(c.getAnnuite()));
			preparedStatement.setString(6, String.valueOf("idk"));
			preparedStatement.setString(7, String.valueOf(c.getIdClient()));
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
		}

		disconnect();
	}

	@Override
	public void register(Client c) {
		
		String query = "INSERT INTO `TClient` (`NumCli`, `NomCli`, `password`) VALUES (NULL, ?, ?);";

		try {
			PreparedStatement preparedStatement = getConnection().prepareStatement(query);
			
			preparedStatement.setString(1, String.valueOf(c.getName()));
			preparedStatement.setString(2, String.valueOf(c.getPassword()));
			
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
		}

		disconnect();
		
	}

}
