package com.sia.anass.mvc.dao;

import com.sia.anass.mvc.model.Client;
import com.sia.anass.mvc.model.CreditModel;

public interface ClientDao {

	public Client login(Client c);
	
	public void insertCredit(CreditModel c);
	
	public void register(Client c);
}
