package com.sia.anass.dbconfig;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.mysql.jdbc.Driver;

public class DBConnection {
	private static String mysqlURL, username, password;

	
	private Connection connect = null;
	
	static {
		Properties properties = new Properties();
		InputStream inputStream = DBConnection.class.getClassLoader().getResourceAsStream("config.properties");
		try {
			properties.load(inputStream);
			mysqlURL = properties.getProperty("url").trim();
			username = properties.getProperty("username").trim();
			password = properties.getProperty("password").trim();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName(Driver.class.getName());
			// Setup the connection with the DB
			connect = DriverManager.getConnection(mysqlURL, username, password);
		} catch (ClassNotFoundException e) {
			// TODO: handle exception
		}catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		return connect;
	}

	public void disconnect() {
		if (connect != null) {
			try {
				connect.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
