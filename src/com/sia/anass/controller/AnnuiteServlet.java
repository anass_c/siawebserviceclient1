package com.sia.anass.controller;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.AxisFault;

import sia.anass.CreditStub;
import sia.anass.CreditStub.Annuite;
import sia.anass.CreditStub.AnnuiteResponse;

/**
 * Servlet implementation class AnnuiteServlet
 */
public class AnnuiteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AnnuiteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
//		System.out.println(((Client)session.getAttribute("client")).getName());
		if (session != null) {
			if (session.getAttribute("isUserLoggedIn") != null && (boolean) session.getAttribute("isUserLoggedIn")) {
				request.getRequestDispatcher("WEB-INF/views/annuite.jsp").forward(request, response);
			} else
				response.sendRedirect("Login");
		} else
			response.sendRedirect("Login");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		caculateAnnuite(request, response, session);
		doGet(request, response);
	}

	private void caculateAnnuite(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		AnnuiteResponse res = null;

		double capital = Double.valueOf(request.getParameter("capital"));
		int aDuree = Integer.valueOf(request.getParameter("duree"));

		double aTaux = Double.valueOf(request.getParameter("taux"));

		try {
			CreditStub stub = new CreditStub();

			Annuite annuite = new Annuite();
			annuite.setCapital(capital);
			annuite.setDuree(aDuree);
			annuite.setTaux(aTaux);

			res = stub.annuite(annuite);

		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		String result = res.get_return().toString();
		request.setAttribute("annuiteRes", result);
	}

}
