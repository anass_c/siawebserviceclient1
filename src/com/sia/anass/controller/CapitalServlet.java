package com.sia.anass.controller;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.AxisFault;

import sia.anass.CreditStub;
import sia.anass.CreditStub.Capital;
import sia.anass.CreditStub.CapitalResponse;

/**
 * Servlet implementation class CapitalServlet
 */
public class CapitalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CapitalServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
//		System.out.println(((Client)session.getAttribute("client")).getName());
		if (session != null) {
			if (session.getAttribute("isUserLoggedIn") != null && (boolean) session.getAttribute("isUserLoggedIn")) {
				request.getRequestDispatcher("WEB-INF/views/capital.jsp").forward(request, response);
			} else
				response.sendRedirect("Login");
		} else
			response.sendRedirect("Login");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		caculateCapital(request, response, session);
		doGet(request, response);
	}

	private void caculateCapital(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		// TODO Auto-generated method stub
		CapitalResponse cRes = null;

		double annuite = Double.valueOf(request.getParameter("annuite"));
		int cDuree = Integer.valueOf(request.getParameter("duree"));

		double cTaux = Double.valueOf(request.getParameter("taux"));

		try {
			CreditStub stub = new CreditStub();

			Capital capital2 = new Capital();
			capital2.setAnnuite(annuite);
			capital2.setDuree(cDuree);
			capital2.setTaux(cTaux);

			cRes = stub.capital(capital2);

		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		String result = cRes.get_return().toString();
		request.setAttribute("capitalRes", result);

	}

}
