package com.sia.anass.controller;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.AxisFault;

import sia.anass.CreditStub;
import sia.anass.CreditStub.Duree;
import sia.anass.CreditStub.DureeResponse;

/**
 * Servlet implementation class DureeServlet
 */
public class DureeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DureeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
//		System.out.println(((Client)session.getAttribute("client")).getName());
		if (session != null) {
			if (session.getAttribute("isUserLoggedIn") != null && (boolean) session.getAttribute("isUserLoggedIn")) {
				request.getRequestDispatcher("WEB-INF/views/duree.jsp").forward(request, response);
			} else
				response.sendRedirect("Login");
		} else
			response.sendRedirect("Login");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		caculateDuree(request, response, session);
		doGet(request, response);
	}

	private void caculateDuree(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		DureeResponse dRes = null;

		double dAnnuite = Double.valueOf(request.getParameter("annuite"));
		double dCapital = Double.valueOf(request.getParameter("capital"));

		double dTaux = Double.valueOf(request.getParameter("taux"));

		try {
			CreditStub stub = new CreditStub();

			Duree duree = new Duree();
			duree.setAnnuite(dAnnuite);
			duree.setCapital(dCapital);
			duree.setTaux(dTaux);

			dRes = stub.duree(duree);

		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		String result = dRes.get_return().toString();
		request.setAttribute("dureeRes", result);
	}

}
