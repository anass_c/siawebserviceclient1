package com.sia.anass.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sia.anass.mvc.dao.ClientDaoImp;
import com.sia.anass.mvc.model.Client;

/**
 * Servlet implementation class RegisterServlet
 */
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ClientDaoImp clientDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        clientDao = new ClientDaoImp();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		HttpSession session = request.getSession(false);
		if (session != null) {
			if (session.getAttribute("isUserLoggedIn") != null && (boolean) session.getAttribute("isUserLoggedIn"))
				response.sendRedirect("Credit");
			else
				request.getRequestDispatcher("WEB-INF/views/ui-register.jsp").forward(request, response);
		} else
			request.getRequestDispatcher("WEB-INF/views/ui-register.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("Username");
		String password = request.getParameter("Password");

		Client user = new Client();
		user.setName(name);
		user.setPassword(password);

		clientDao.register(user);
		
		doGet(request, response);
	}

}
