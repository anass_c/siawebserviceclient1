package com.sia.anass.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sia.anass.mvc.dao.ClientDao;
import com.sia.anass.mvc.dao.ClientDaoImp;
import com.sia.anass.mvc.model.Client;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ClientDao clientDao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
		clientDao = new ClientDaoImp();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		HttpSession session = request.getSession(false);
		if (session != null) {
			if (session.getAttribute("isUserLoggedIn") != null && (boolean) session.getAttribute("isUserLoggedIn"))
				response.sendRedirect(getServletContext().getContextPath() + "/Annuite");
			else
				request.getRequestDispatcher("WEB-INF/views/ui-login.jsp").forward(request, response);
		} else
			request.getRequestDispatcher("WEB-INF/views/ui-login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("Username");
		String password = request.getParameter("Password");

		Client user = new Client();
		user.setName(name);
		user.setPassword(password);

		Client client = clientDao.login(user);

		if (client != null) {
			HttpSession session = request.getSession(false);
			session.setAttribute("isUserLoggedIn", true);
			session.setAttribute("id", client.getId());
			session.setAttribute("name", client.getName());
			
			

			response.sendRedirect(getServletContext().getContextPath() + "/Annuite");
		} else {

			request.setAttribute("error", "true");
			request.getRequestDispatcher("WEB-INF/views/ui-login.jsp").forward(request, response);
		}
	}

}
