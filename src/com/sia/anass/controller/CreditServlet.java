package com.sia.anass.controller;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.AxisFault;

import com.sia.anass.mvc.dao.ClientDao;
import com.sia.anass.mvc.dao.ClientDaoImp;
import com.sia.anass.mvc.model.CreditModel;

import sia.anass.CreditStub;
import sia.anass.CreditStub.Annuite;
import sia.anass.CreditStub.AnnuiteResponse;
import sia.anass.CreditStub.Capital;
import sia.anass.CreditStub.CapitalResponse;
import sia.anass.CreditStub.Duree;
import sia.anass.CreditStub.DureeResponse;

/**
 * Servlet implementation class CreditServlet
 */
public class CreditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
//		System.out.println(((Client)session.getAttribute("client")).getName());
		if (session != null) {
			if (session.getAttribute("isUserLoggedIn") != null && (boolean) session.getAttribute("isUserLoggedIn")) {
				request.getRequestDispatcher("WEB-INF/views/credit.jsp").forward(request, response);
			} else
				response.sendRedirect("Login");
		} else
			response.sendRedirect("Login");

	}

	private void caculate(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		// TODO Auto-generated method stub

		String result = "";
		switch (request.getParameter("type")) {
		case "annuite":
			AnnuiteResponse res = null;

			double capital = Double.valueOf(request.getParameter("capital"));
			int aDuree = Integer.valueOf(request.getParameter("duree"));

			double aTaux = Double.valueOf(request.getParameter("taux"));

			try {
				CreditStub stub = new CreditStub();

				Annuite annuite = new Annuite();
				annuite.setCapital(capital);
				annuite.setDuree(aDuree);
				annuite.setTaux(aTaux);

				res = stub.annuite(annuite);

			} catch (AxisFault e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			result = res.get_return().toString();
			request.setAttribute("annuiteRes", result);
			break;
		case "capital":

			CapitalResponse cRes = null;

			double annuite = Double.valueOf(request.getParameter("cAnnuite"));
			int cDuree = Integer.valueOf(request.getParameter("cDuree"));

			double cTaux = Double.valueOf(request.getParameter("cTaux"));

			try {
				CreditStub stub = new CreditStub();

				Capital capital2 = new Capital();
				capital2.setAnnuite(annuite);
				capital2.setDuree(cDuree);
				capital2.setTaux(cTaux);

				cRes = stub.capital(capital2);

			} catch (AxisFault e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			result = cRes.get_return().toString();
			request.setAttribute("capitalRes", result);
			break;
		case "duree":
			DureeResponse dRes = null;

			double dAnnuite = Double.valueOf(request.getParameter("dAnnuite"));
			double dCapital = Double.valueOf(request.getParameter("dCapital"));

			double dTaux = Double.valueOf(request.getParameter("dTaux"));

			try {
				CreditStub stub = new CreditStub();

				Duree duree = new Duree();
				duree.setAnnuite(dAnnuite);
				duree.setCapital(dCapital);
				duree.setTaux(dTaux);

				dRes = stub.duree(duree);

			} catch (AxisFault e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			result = dRes.get_return().toString();
			request.setAttribute("dureeRes", result);
			break;
		case "sauvgarder":
			ClientDao clientDao = new ClientDaoImp();

			CreditModel creditModel = new CreditModel();
			creditModel.setIdClient((int) session.getAttribute("id"));
			creditModel.setCapital(request.getParameter("capital"));
			creditModel.setAnnuite(request.getParameter("annuite"));
			creditModel.setTaux(request.getParameter("taux"));
			creditModel.setDuree(request.getParameter("duree"));

			clientDao.insertCredit(creditModel);
			

			break;

		default:
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		caculate(request, response, session);

		doGet(request, response);
	}

}
