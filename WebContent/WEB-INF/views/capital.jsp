<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html class=" ">

<head>
<!-- 
         * @Package: Ultra Admin - Responsive Theme
         * @Subpackage: Bootstrap
         * @Version: 4.1
         * This file is part of Ultra Admin Theme.
        -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />

<link rel="shortcut icon" href="assets/images/favicon.png"
	type="image/x-icon" />
<!-- Favicon -->
<link rel="apple-touch-icon-precomposed"
	href="assets/images/apple-touch-icon-57-precomposed.png">
<!-- For iPhone -->
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="assets/images/apple-touch-icon-114-precomposed.png">
<!-- For iPhone 4 Retina display -->
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="assets/images/apple-touch-icon-72-precomposed.png">
<!-- For iPad -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="assets/images/apple-touch-icon-144-precomposed.png">
<!-- For iPad Retina display -->
<!-- CORE CSS FRAMEWORK - START -->
<link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="assets/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap/css/bootstrap-theme.min.css"
	rel="stylesheet" type="text/css" />
<link href="assets/fonts/font-awesome/css/font-awesome.css"
	rel="stylesheet" type="text/css" />
<link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css"
	rel="stylesheet" type="text/css" />
<!-- CORE CSS FRAMEWORK - END -->

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


<!-- CORE CSS TEMPLATE - START -->
<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.loan-emi {
	padding-top: 5px;
	color: #fff;
}

.single-total h2 {
	color: #232935;
	font-size: 20px;
	font-weight: 600;
}

.single-total h2.emi-price {
	font-size: 50px;
}

.single-total h5 {
	font-size: 20px;
	font-weight: 600;
}

.single-total {
	margin-bottom: 0px;
}
</style>
<!-- CORE CSS TEMPLATE - END -->

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class=" ">
	<!-- START TOPBAR -->
	<div class='page-topbar '>
		<div class='logo-area'></div>
		<div class='quick-area'>
			<div class='pull-left'>
				<ul class="info-menu left-links list-inline list-unstyled">
					<li class="sidebar-toggle-wrap"><a href="#"
						data-toggle="sidebar" class="sidebar_toggle"> <i
							class="fa fa-bars"></i>
					</a></li>
				</ul>
			</div>
			<div class='pull-right'>
				<ul class="info-menu right-links list-inline list-unstyled">
					<li class="profile"><a href="#" data-toggle="dropdown"
						class="toggle"> <img src="data/profile/profile.png"
							alt="user-image" class="img-circle img-inline"> <span>${sessionScope.name}<i
								class="fa fa-angle-down"></i>
						</span>
					</a>
						<ul class="dropdown-menu profile animated fadeIn">
							<li><a href="#settings"> <i class="fa fa-wrench"></i>
									Settings
							</a></li>
							<li><a href="#profile"> <i class="fa fa-user"></i>
									Profile
							</a></li>
							<li><a href="#help"> <i class="fa fa-info"></i> Help
							</a></li>
							<li class="last"><a href="Logout"> <i class="fa fa-lock"></i>
									Logout
							</a></li>
						</ul></li>

				</ul>
			</div>
		</div>

	</div>
	<!-- END TOPBAR -->
	<!-- START CONTAINER -->
	<div class="page-container row-fluid">

		<!-- SIDEBAR - START -->
		<div class="page-sidebar ">

			<!-- MAIN MENU - START -->
			<div class="page-sidebar-wrapper" id="main-menu-wrapper">

				<!-- USER INFO - START -->
				<div class="profile-info row">

					<div class="profile-image col-md-4 col-sm-4 col-xs-4">
						<a href="ui-profile.html"> <img src="data/profile/profile.png"
							class="img-responsive img-circle">
						</a>
					</div>

					<div class="profile-details col-md-8 col-sm-8 col-xs-8">

						<h3>
							<a href="ui-profile.html">${sessionScope.name}</a>

							<!-- Available statuses: online, idle, busy, away and offline -->
							<span class="profile-status online"></span>
						</h3>

						<p class="profile-title">Client</p>

					</div>

				</div>
				<!-- USER INFO - END -->



				<ul class='wraplist'>


					<li class="open"><a href="javascript:;"> <i
							class="fa fa-suitcase"></i> <span class="title">Simulation
								credit</span> <span class="arrow"></span>
					</a>
						<ul class="sub-menu" style="display: none;">
							<li><a class="" href="Annuite">Annuite</a></li>
							<li><a class="active" href="Capital">Capital</a></li>
							<li><a class="" href="Duree">Duree</a></li>
						</ul></li>
				</ul>

			</div>
			<!-- MAIN MENU - END -->





		</div>
		<!--  SIDEBAR - END -->
		<!-- START CONTENT -->
		<section id="main-content" class=" ">
			<section class="wrapper main-wrapper" style=''>

				<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
					<div class="page-title">

						<div class="pull-left">
							<h1 class="title">Capital</h1>
						</div>



					</div>
				</div>
				<div class="clearfix"></div>

				<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
					<section class="box ">
						<header class="panel_header">
							<h2 class="title pull-left">Capital</h2>

						</header>
						<div class="content-body">
							<div class="row">
								<div class="col-md-8 col-sm-9 col-xs-10">
									<form action="" method="post" class="needs-validation">
										<div class="form-group has-danger">
											<label class="form-label" for="field-1">Annuite</label> <span
												class="desc">e.g. 100,000.00</span>
											<div class="controls">
												<input type="number" class="form-control" id="field-1"
													min="0" step="0.01" name="annuite" required="required"
													value="${param.annuite}">
											</div>
										</div>

										<div class="form-group">
											<label class="form-label" for="field-2">Taux</label> <span
												class="desc">e.g. "3.7"</span>
											<div class="controls">
												<input type="number" class="form-control" id="field-2"
													min="0" step="0.01" name="taux" required="required"
													value="${param.taux}">
											</div>
										</div>

										<div class="form-group">
											<label class="form-label" for="field-3">Duree</label> <span
												class="desc">e.g. "43"</span>
											<div class="controls">
												<input type="number" class="form-control" id="field-3"
													name="duree" required="required" min="1"
													value="${param.duree}">
											</div>
										</div>


										<div class="form-group">
											<div class="controls">
												<input type="submit" class="btn btn-primary form-control"
													value="Calculer" />
											</div>
										</div>

									</form>

								</div>
								<div
									class="col-md-4 col-sm-5 col-xs-6 text-center loan-emi primary">
									<div class="total-calculation">
										<div class="single-total">
											<h5>Capital</h5>
											<h2 class="emi-price" id="emi">


												<c:choose>

													<c:when test="${not empty capitalRes}">
														<span class="animated fadeIn">${capitalRes}</span>
													</c:when>
													<c:otherwise>
														<span class="animated fadeIn">00.0</span>
													</c:otherwise>
												</c:choose>

											</h2>
										</div>
										<div class="single-total">
											<h5>Total des int�r�ts � payer</h5>
											<h2 id="tbl_emi">
												<c:choose>
													<c:when test="${not empty capitalRes}">
														<span class="animated fadeIn"> <fmt:formatNumber
																type="number" maxFractionDigits="2"
																value="${(param.annuite * param.duree) - capitalRes}" /></span>
													</c:when>
													<c:otherwise>
														<span class="animated fadeIn">00.0</span>
													</c:otherwise>
												</c:choose>
											</h2>
										</div>
										<div class="single-total">
											<h5>
												Paiement total <br> (Principal + int�r�t)
											</h5>
											<h2 id="tbl_la">
												<c:choose>
													<c:when test="${not empty capitalRes}">
														<span class="animated fadeIn"> <fmt:formatNumber
																type="number" maxFractionDigits="2"
																value="${(param.annuite * param.duree)}" /></span>
													</c:when>
													<c:otherwise>
														<span class="animated fadeIn">00.0</span>
													</c:otherwise>
												</c:choose>
											</h2>
										</div>
										<div class="single-total">
											<c:choose>

												<c:when test="${not empty capitalRes}">
													<form method="post">

														<input type="hidden" name="annuite" value="${param.annuite}">
														<input type="hidden" name="capital"
															value="${capitalRes}"> <input type="hidden"
															name="taux" value="${param.taux}"> <input
															type="hidden" name="duree" value="${param.duree}">
														<input type="submit" name="type" value="sauvgarder"
															class="btn btn-primary animated fadeIn">
													</form>
												</c:when>
											</c:choose>
										</div>

									</div>

								</div>
							</div>


						</div>
					</section>
				</div>



			</section>
		</section>
		<!-- END CONTENT -->

		<div class="chatapi-windows "></div>
	</div>
	<!-- END CONTAINER -->
	<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


	<!-- CORE JS FRAMEWORK - START -->
	<script src="assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
	<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"
		type="text/javascript"></script>
	<script src="assets/plugins/viewport/viewportchecker.js"
		type="text/javascript"></script>
	<!-- CORE JS FRAMEWORK - END -->


	<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
	<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


	<!-- CORE TEMPLATE JS - START -->
	<script src="assets/js/scripts.js" type="text/javascript"></script>
	<!-- END CORE TEMPLATE JS - END -->

	<!-- Sidebar Graph - START -->
	<script src="assets/plugins/sparkline-chart/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script src="assets/js/chart-sparkline.js" type="text/javascript"></script>
	<!-- Sidebar Graph - END -->

	<!-- General section box modal start -->
	<div class="modal" id="section-settings" tabindex="-1" role="dialog"
		aria-labelledby="ultraModal-Label" aria-hidden="true">
		<div class="modal-dialog animated bounceInDown">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Section Settings</h4>
				</div>
				<div class="modal-body">Body goes here...</div>
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					<button class="btn btn-success" type="button">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- modal end -->
</body>

</html>



