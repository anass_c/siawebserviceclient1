<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Web Services Project</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>
.center_div {
	margin: 50px auto;
	width: 50% /* value of your choice which suits your alignment */
}
</style>
</head>
<body>

	<section>
		<div class="text-center">
			<img src="${pageContext.request.contextPath}/images/safebox.svg"
				width="100" height="100" class="ounded mx-auto d-block" alt="" />
			<h3>IAO Bank</h3>

		</div>

		<div class="container center_div">
			<div id="login" class="container center_div tab-pane fade in active">
				<form id="myForm" method="post" action="Login">

					<c:if test="${error == 'true'}">
						<div class="alert alert-danger animated fadeIn" role="alert">User name or password is incorrect!!</div>
					</c:if>
					<div class="form-group">
						<label for="nameInput">Nom </label> <input name="Name" type="text"
							class="form-control" id="nameInput" placeholder="Inserer Nom">
					</div>
					<div class="form-group">
						<label for="passwordInput">Mot de pass</label> <input
							name="Password" type="password" class="form-control"
							id="tauxInput" placeholder="Inserer mot de pass">
					</div>

					<button type="submit" class="btn btn-primary">Connexion</button>
				</form>
			</div>
		</div>
	</section>



</body>
</html>
