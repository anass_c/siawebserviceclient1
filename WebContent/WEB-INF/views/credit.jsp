<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.mysql.jdbc.Driver"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Web Services Projet</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">



<!--  <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

<style>
body {
	line-height: 26px;
}

.slidecontainer {
	width: 100%;
}

.slider {
	-webkit-appearance: none;
	width: 100%;
	height: 25px;
	background: #d3d3d3;
	outline: none;
	opacity: 0.7;
	-webkit-transition: .2s;
	transition: opacity .2s;
}

.slider:hover {
	opacity: 1;
}

.slider::-webkit-slider-thumb {
	-webkit-appearance: none;
	appearance: none;
	width: 25px;
	height: 25px;
	background: #286090;
	cursor: pointer;
}

.slider::-moz-range-thumb {
	width: 25px;
	height: 25px;
	background: #286090;
	cursor: pointer;
}

section {
	margin-bottom: 10%;
}

.loan-emi {
	background: #3a7cdd;
	color: #fff;
	padding: 30px;
}

.single-total h2 {
	color: #232935;
	font-size: 20px;
	font-weight: 600;
}

.single-total h2.emi-price {
	font-size: 50px;
}

.single-total h5 {
	font-size: 20px;
	font-weight: 600;
}

.single-total {
	margin-bottom: 30px;
}
</style>

<!--  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>

<!--<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
</head>
<body>
	<header>

		<!--<nav class="navbar bg-primary ">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#"> <img src="safebox.svg"
						width="30" height="30" class="d-inline-block align-top" alt="" />
					</a>
				</div>
			</div>
		</nav>-->
		<!-- Navigation -->
		<nav class="navbar navbar-light bg-light justify-content-between">
			<a class="navbar-brand"><img
				src="${pageContext.request.contextPath}/images/safebox.svg"
				width="40" height="40" class="d-inline-block align-center" alt="" />IAO
				Bank</a>
			<h5 class="text-center">Bonjour ${sessionScope.name}</h5>
			<form class="form-inline" action="Logout">

				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Autre
					client</button>
			</form>
		</nav>
	</header>
	<section>

		<div class="container">

			<h2>Web Services Projet</h2>
			
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item"><a
					class="nav-link ${empty param.type ? 'active' : param.type=='annuite' ? 'active':''}"
					id="home-tab" data-toggle="tab" href="#annuite" role="tab"
					aria-controls="home" aria-selected="true">Annuite</a></li>
				<li class="nav-item"><a
					class="nav-link ${param.type=='capital' ? 'active':''}"
					id="profile-tab" data-toggle="tab" href="#capital" role="tab"
					aria-controls="profile" aria-selected="false">Capital</a></li>
				<li class="nav-item"><a
					class="nav-link ${param.type=='duree' ? 'active':''}"
					id="contact-tab" data-toggle="tab" href="#duree" role="tab"
					aria-controls="contact" aria-selected="false">Duree</a></li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div
					class="tab-pane fade ${empty param.type ? 'show active' : param.type=='annuite' ? 'show active':''}"
					id="annuite" role="tabpanel" aria-labelledby="home-tab">
					<div class="row">
						<div class="col-md-8">
							<h3>Annuite</h3>
							<form id="myForm" method="post" class="needs-validation">
								<input type="hidden" name="type" value="annuite" />
								<div class="form-group has-danger">
									<label for="Input1">Capital: (en DH) </label> <input
										value="${param.capital}" required="required" name="capital"
										type="number" min="0" step="0.01" class="form-control"
										id="Input1" aria-describedby="emailHelp"
										placeholder="Inserer Capital">
									<div class="invalid-feedback">Please fill out this field.</div>
								</div>
								<div class="form-group">
									<label for="Input2">Taux: (en %)</label> <input name="taux"
										type="number" required="required" min="0" step="0.01"
										value="${param.taux}" class="form-control" id="Input2"
										placeholder="Inserer Taux">
									<div class="invalid-feedback">Please fill out this field.</div>
								</div>


								<div class="form-group slidecontainer">
									<label for="atInput">Duree: <span id="atDemo"></span>
										(mois)
									</label> <input name="duree" required="required" type="range" min="12"
										max="100" value="${empty param.duree ? 50 : param.duree}"
										class="slider" id="atInput">
								</div>

								<button type="submit" class="sauvgarder btn btn-primary"
									name="annuite">Calculer</button>
							</form>
						</div>

						<div class="col-md-4 text-center loan-emi">
							<div class="total-calculation">
								<div class="single-total">
									<h5>Annuite</h5>
									<h2 class="emi-price" id="emi">


										<c:choose>

											<c:when test="${not empty annuiteRes}">
												<span class="animated fadeIn">${annuiteRes}</span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">00.0</span>
											</c:otherwise>
										</c:choose>

									</h2>
								</div>
								<div class="single-total">
									<h5>Total des intérêts à payer</h5>
									<h2 id="tbl_emi">
										<c:choose>
											<c:when test="${not empty annuiteRes}">
												<span class="animated fadeIn"> <fmt:formatNumber
														type="number" maxFractionDigits="2"
														value="${(annuiteRes * param.duree/12) - param.capital}" /></span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">00.0</span>
											</c:otherwise>
										</c:choose>
									</h2>
								</div>
								<div class="single-total">
									<h5>
										Paiement total <br> (Principal + intérêt)
									</h5>
									<h2 id="tbl_la">
										<c:choose>
											<c:when test="${not empty annuiteRes}">
												<span class="animated fadeIn"> <fmt:formatNumber
														type="number" maxFractionDigits="2"
														value="${(annuiteRes * param.duree/12)}" /></span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">00.0</span>
											</c:otherwise>
										</c:choose>
									</h2>
								</div>
								<div class="single-total">
									<c:choose>

										<c:when test="${not empty annuiteRes}">
											<form method="post">
												
												<input type="hidden" name="annuite" value="${annuiteRes}" >
												<input type="hidden" name="capital" value="${param.capital}" >
												<input type="hidden" name="taux" value="${param.taux}" >
												<input type="hidden" name="duree" value="${param.duree}" >
												<input type="submit" name="type" value="sauvgarder" class="btn btn-success animated fadeIn">
											</form>
										</c:when>
									</c:choose>
								</div>

							</div>
						</div>
					</div>


				</div>
				<div
					class="tab-pane fade ${param.type=='capital' ? 'show active':''}"
					id="capital" role="tabpanel" aria-labelledby="profile-tab">
					<div class="row">
						<div class="col-md-8">
							<h3>Capital</h3>
							<form id="myForm" method="post" class="needs-validation">
								<input type="hidden" name="type" value="capital" />
								<div class="form-group has-danger">
									<label for="annuiteInput">Annuite: (en DH) </label> <input
										value="${param.cAnnuite}" required="required" name="cAnnuite"
										type="number" min="0" step="0.01" class="form-control"
										id="Input1" aria-describedby="emailHelp"
										placeholder="Inserer Capital">
									<div class="invalid-feedback">Please fill out this field.</div>
								</div>
								<div class="form-group">
									<label for="tauxInput">Taux: (en %)</label> <input name="cTaux"
										value="${param.cTaux}" type="number" required="required"
										min="0" step="0.01" class="form-control" id="tauxInput"
										placeholder="Inserer Taux">
									<div class="invalid-feedback">Please fill out this field.</div>
								</div>


								<div class="form-group slidecontainer">
									<label for="ctInput">Duree: <span id="ctDemo"></span>
										(mois)
									</label> <input name="cDuree" required="required" type="range" min="12"
										max="100" value="${empty param.cDuree ? 50 : param.cDuree}"
										class="slider" id="ctInput">
								</div>

								<button type="submit" class="sauvgarder btn btn-primary">Calculer</button>
							</form>
						</div>

						<div class="col-md-4 text-center loan-emi">
							<div class="total-calculation">
								<div class="single-total">
									<h5>Capital</h5>
									<h2 class="emi-price" id="emi">


										<c:choose>

											<c:when test="${not empty capitalRes}">
												<span class="animated fadeIn">${capitalRes}</span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">00.0</span>
											</c:otherwise>
										</c:choose>

									</h2>
								</div>
								<div class="single-total">
									<h5>Total des intérêts à payer</h5>
									<h2 id="tbl_emi">
										<c:choose>
											<c:when test="${not empty capitalRes}">
												<span class="animated fadeIn"> <fmt:formatNumber
														type="number" maxFractionDigits="2"
														value="${(param.cAnnuite * param.cDuree/12) - capitalRes}" /></span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">00.0</span>
											</c:otherwise>
										</c:choose>
									</h2>
								</div>
								<div class="single-total">
									<h5>
										Paiement total <br> (Principal + intérêt)
									</h5>
									<h2 id="tbl_la">
										<c:choose>
											<c:when test="${not empty capitalRes}">
												<span class="animated fadeIn"> <fmt:formatNumber
														type="number" maxFractionDigits="2"
														value="${(param.cAnnuite * param.cDuree/12)}" /></span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">00.0</span>
											</c:otherwise>
										</c:choose>
									</h2>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade ${param.type=='duree' ? 'show active':''}"
					id="duree" role="tabpanel" aria-labelledby="contact-tab">
					<div class="row">
						<div class="col-md-8">
							<h3>Duree</h3>
							<form id="myForm" method="post" class="needs-validation">
								<input type="hidden" name="type" value="duree" />
								<div class="form-group has-danger">
									<label for="annuiteInput">Capital: (en DH) </label> <input
										value="${param.dCapital}" required="required" name="dCapital"
										type="number" min="0" step="0.01" class="form-control"
										id="Input1" aria-describedby="emailHelp"
										placeholder="Inserer Capital">
									<div class="invalid-feedback">Please fill out this field.</div>
								</div>
								<div class="form-group has-danger">
									<label for="annuiteInput">Annuite: (en DH) </label> <input
										value="${param.dAnnuite}" required="required" name="dAnnuite"
										type="number" min="0" step="0.01" class="form-control"
										id="Input1" aria-describedby="emailHelp"
										placeholder="Inserer Capital">
									<div class="invalid-feedback">Please fill out this field.</div>
								</div>
								<div class="form-group">
									<label for="tauxInput">Taux: (en %)</label> <input name="dTaux"
										value="${param.dTaux}" type="number" required="required"
										min="0" step="0.01" class="form-control" id="tauxInput"
										placeholder="Inserer Taux">
									<div class="invalid-feedback">Please fill out this field.</div>
								</div>

								<button type="submit" class="sauvgarder btn btn-primary">Calculer</button>
							</form>
						</div>

						<div class="col-md-4 text-center loan-emi">
							<div class="total-calculation">
								<div class="single-total">
									<h5>Duree</h5>
									<h2 class="emi-price" id="emi">


										<c:choose>

											<c:when test="${not empty dureeRes}">
												<span class="animated fadeIn">${dureeRes} mois</span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">0 mois</span>
											</c:otherwise>
										</c:choose>

									</h2>
								</div>
								<div class="single-total">
									<h5>Total des intérêts à payer</h5>
									<h2 id="tbl_emi">
										<c:choose>
											<c:when test="${not empty dureeRes}">
												<span class="animated fadeIn"> <fmt:formatNumber
														type="number" maxFractionDigits="2"
														value="${(param.dAnnuite * dureeRes/12) - param.dCapital}" /></span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">00.0</span>
											</c:otherwise>
										</c:choose>
									</h2>
								</div>
								<div class="single-total">
									<h5>
										Paiement total <br> (Principal + intérêt)
									</h5>
									<h2 id="tbl_la">
										<c:choose>
											<c:when test="${not empty dureeRes}">
												<span class="animated fadeIn"> <fmt:formatNumber
														type="number" maxFractionDigits="2"
														value="${(param.dAnnuite * dureeRes/12)}" /></span>
											</c:when>
											<c:otherwise>
												<span class="animated fadeIn">00.0</span>
											</c:otherwise>
										</c:choose>
									</h2>
								</div>

							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</section>

	<div id="dataModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="text-center modal-title">Annuite</h3>
				</div>
				<div class="modal-body" id="employee_detail"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		var atSlider = document.getElementById("atInput");
		var atOutput = document.getElementById("atDemo");
		atOutput.innerHTML = atSlider.value;

		atSlider.oninput = function() {
			atOutput.innerHTML = this.value;
		}

		var ctSlider = document.getElementById("ctInput");
		var ctOutput = document.getElementById("ctDemo");
		ctOutput.innerHTML = ctSlider.value;

		ctSlider.oninput = function() {
			ctOutput.innerHTML = this.value;
		}
	</script>

</body>
</html>
